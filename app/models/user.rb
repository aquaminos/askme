require 'openssl'

class User < ApplicationRecord
  # Параметры для модуля шифрования
  ITERATIONS = 20000
  DIGEST = OpenSSL::Digest::SHA256.new

  has_many :questions

  validates :email, :username, presence: true
  validates :email, :username, uniqueness: true

  validates :username, format: { with: /\A[a-z]+\w*\z/ }
  validates :username, length: { maximum: 40 }
  validates :email, email_format: {}

  attr_accessor :password
  validates :password, presence: true, on: :create
  validates :password, confirmation: true

  before_validation :username_to_downcase

  def username_to_downcase
    self.username = username.downcase unless username == nil
  end

  before_save :encrypt_password

  def encrypt_password
    if password.present?
      self.password_salt = User.hash_to_string(OpenSSL::Random.random_bytes(16))
      self.password_hash = User.hash_to_string(
        OpenSSL::PKCS5.pbkdf2_hmac(password,
                                   password_salt,
                                   ITERATIONS, DIGEST.length, DIGEST)
      )
    end
  end

  def self.hash_to_string(password_hash)
    password_hash.unpack('H*')[0]
  end

  def self.authenticate(email, password)
    user = find_by(email: email)
    return nil unless user.present?

    hashed_password = User.hash_to_string(
      OpenSSL::PKCS5.pbkdf2_hmac(
        password, user.password_salt, ITERATIONS, DIGEST.length, DIGEST
      )
    )
    return user if user.password_hash == hashed_password
    nil
  end
end
